package com.suprix.hrsytem.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.suprix.hrsytem.user.dao.UserDao;
import com.suprix.hrsytem.user.model.User;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserDao userDao;
	
	@Override
	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public User findByEmail(String email) {
		return userDao.findByEmail(email);
	}

	@Override
	public User findById(String id) {
		return userDao.findById(id);
	}

	@Override
	public List<User> findAll() {
		return userDao.findAll();

	}

	@Override
	public void save(User user) {
		userDao.save(user);
		
	}

	@Override
	public void deleteById(String id) {
		userDao.deleteById(id);
		
	}

}
