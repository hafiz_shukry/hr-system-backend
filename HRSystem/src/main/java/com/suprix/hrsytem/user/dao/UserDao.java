package com.suprix.hrsytem.user.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.suprix.hrsytem.user.model.User;

@Transactional
public interface UserDao extends CrudRepository<User, Long>{

	User findByUsername(String username);
	User findByEmail(String email);
	User findById(String id);
	List<User> findAll();


	@Modifying
	@Query("DELETE from User where id = :id")
	void deleteById(@Param("id") String id);

}
