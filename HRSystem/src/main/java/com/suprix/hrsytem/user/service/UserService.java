package com.suprix.hrsytem.user.service;

import java.util.List;

import com.suprix.hrsytem.user.model.User;

public interface UserService {

	User findByUsername(String username);
	User findByEmail(String email);
	User findById(String id);

	List<User> findAll();
	
	void save(User user);
	void deleteById(String id);

}
