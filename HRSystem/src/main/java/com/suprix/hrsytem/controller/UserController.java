package com.suprix.hrsytem.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.suprix.hrsytem.user.model.User;
import com.suprix.hrsytem.user.service.UserService;
import com.suprix.hrsytem.util.UUIDGenerator;

@RestController
@RequestMapping("user")
public class UserController {

	public static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	// register
	@RequestMapping(value ="/register", method = RequestMethod.POST)
	public ResponseEntity<?> register(@RequestBody User user) {
		logger.info("Creating User : {}", user);
		
		user.setId(UUIDGenerator.getInstance().getId());
		user.setUsername(user.getEmail());
		userService.save(user);
		
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}
 
}
