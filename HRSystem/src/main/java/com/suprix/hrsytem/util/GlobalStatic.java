package com.suprix.hrsytem.util;

public class GlobalStatic {

	public static final String ROLE_ADMIN_ID = "ADMIN";
	public static final String ROLE_STAFF_ID = "STAFF";
	
	public static final String ERR_EXIST_USERNAME = "ERR_EXIST_USERNAME";
	public static final String ERR_EXIST_EMAIL = "ERR_EXIST_EMAIL";
	public static final String ERR_EXIST_EMAIL_USERNAME = "ERR_EXIST_EMAIL_USERNAME";
	public static final String ERR_WRONGPASSWORD = "ERR_WRONGPASSWORD";
	public static final String ERR_SAMEOLDPASSWORD = "ERR_SAMEOLDPASSWORD";
}
